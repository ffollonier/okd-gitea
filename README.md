# Gitea for OpenShift

> forked from: https://github.com/wkulhanek/docker-openshift-gitea
> and switch from a Centos 7 base to a RHEL 7 one.

## Description

Gitea is a Git service. Learn more about it at https://gitea.io/en-US/

Running containers on OpenShift comes with certain security and other requirements. This repository contains:

* A Dockerfile for building an OpenShift-compatible Gitea image
* The run scripts used in the Docker image

## Create the buildconfig

```bash
$ oc new-build https://github.com/ffollonier/okd-gitea.git
--> Creating resources with label build=okd-gitea ...
    imagestream.image.openshift.io "centos" created
    imagestream.image.openshift.io "okd-gitea" created
    buildconfig.build.openshift.io "okd-gitea" created
--> Success
```

## History

| Date          | Author            | Comment                                   |
| ------------- |:-----------------:|:------------------------------------------|
| 2018-11-22    | Follonier Fred    | Initial version                           |
