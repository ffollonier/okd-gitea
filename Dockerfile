FROM docker.io/centos:7

# Set the Gitea Version to install.
# Check https://dl.gitea.io/gitea/ for available versions.
ENV GITEA_VERSION="1.8.1"
ENV APP_HOME=/home/gitea
ENV DATA_HOME=/home/gitea/data

LABEL name="Gitea - Git Service" \
      vendor="Gitea" \
      io.k8s.display-name="Gitea - Git Service" \
      io.openshift.expose-services="3000,gitea" \
      io.openshift.tags="gitea" \
      version=$GITEA_VERSION \
      maintainer="Follonier Fred <fred@follonier.net>"

COPY ./root /

# Update latest packages and install Prerequisites
RUN yum -y update && yum -y upgrade \
    && yum -y install git asciidoc ca-certificates \
              gettext openssh s6 su-exec tzdata \
    && yum -y clean all \
    && rm -rf /var/cache/yum

RUN adduser gitea --home-dir=/home/gitea \
    && mkdir ${DATA_HOME} \
    && chmod 775 ${DATA_HOME} \
    && chgrp 0 ${DATA_HOME} \
    && mkdir -p ${APP_HOME}/data/lfs \
    && mkdir -p ${APP_HOME}/conf \
    && mkdir /.ssh \
    && curl -L -o ${APP_HOME}/gitea https://dl.gitea.io/gitea/${GITEA_VERSION}/gitea-${GITEA_VERSION}-linux-amd64 \
    && chmod 775 ${APP_HOME}/gitea \
    && chown gitea:root ${APP_HOME}/gitea \
    && chgrp -R 0 ${APP_HOME} \
    && chgrp -R 0 /.ssh \
    && chmod -R g=u ${APP_HOME} /etc/passwd

COPY ./app.ini ${APP_HOME}/conf

RUN chown gitea:root ${APP_HOME}/conf/app.ini

WORKDIR ${APP_HOME}
VOLUME ${DATA_HOME}
EXPOSE 22 3000
USER 1001

ENTRYPOINT ["/usr/bin/rungitea"]
